/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *consoleLayout;
    QLabel *consoleLBL;
    QListWidget *consoleLW;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *infoLayout;
    QLabel *pInfoLBL;
    QLineEdit *nameLE;
    QLineEdit *surnameLE;
    QLabel *fInfoLBL;
    QLineEdit *cnameLE;
    QLineEdit *cadressLE;
    QSpacerItem *verticalSpacer;
    QPushButton *addPBTN;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *mngrLayout;
    QLabel *sqlLBL;
    QListWidget *mngrLW;
    QLineEdit *sqlLE;
    QPushButton *mngrBTN;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *buttonLayout;
    QPushButton *gateReadBTN;
    QPushButton *gateWriteBTN;
    QPushButton *rfidReadBTN;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(700, 560);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 691, 391));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        consoleLayout = new QVBoxLayout();
        consoleLayout->setSpacing(6);
        consoleLayout->setObjectName(QString::fromUtf8("consoleLayout"));
        consoleLBL = new QLabel(gridLayoutWidget);
        consoleLBL->setObjectName(QString::fromUtf8("consoleLBL"));

        consoleLayout->addWidget(consoleLBL);

        consoleLW = new QListWidget(gridLayoutWidget);
        consoleLW->setObjectName(QString::fromUtf8("consoleLW"));

        consoleLayout->addWidget(consoleLW);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        consoleLayout->addItem(horizontalSpacer);


        gridLayout->addLayout(consoleLayout, 1, 1, 1, 1);

        infoLayout = new QVBoxLayout();
        infoLayout->setSpacing(6);
        infoLayout->setObjectName(QString::fromUtf8("infoLayout"));
        pInfoLBL = new QLabel(gridLayoutWidget);
        pInfoLBL->setObjectName(QString::fromUtf8("pInfoLBL"));

        infoLayout->addWidget(pInfoLBL);

        nameLE = new QLineEdit(gridLayoutWidget);
        nameLE->setObjectName(QString::fromUtf8("nameLE"));

        infoLayout->addWidget(nameLE);

        surnameLE = new QLineEdit(gridLayoutWidget);
        surnameLE->setObjectName(QString::fromUtf8("surnameLE"));

        infoLayout->addWidget(surnameLE);

        fInfoLBL = new QLabel(gridLayoutWidget);
        fInfoLBL->setObjectName(QString::fromUtf8("fInfoLBL"));

        infoLayout->addWidget(fInfoLBL);

        cnameLE = new QLineEdit(gridLayoutWidget);
        cnameLE->setObjectName(QString::fromUtf8("cnameLE"));

        infoLayout->addWidget(cnameLE);

        cadressLE = new QLineEdit(gridLayoutWidget);
        cadressLE->setObjectName(QString::fromUtf8("cadressLE"));

        infoLayout->addWidget(cadressLE);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        infoLayout->addItem(verticalSpacer);

        addPBTN = new QPushButton(gridLayoutWidget);
        addPBTN->setObjectName(QString::fromUtf8("addPBTN"));

        infoLayout->addWidget(addPBTN);


        gridLayout->addLayout(infoLayout, 1, 0, 1, 1);

        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(350, 390, 341, 146));
        mngrLayout = new QVBoxLayout(verticalLayoutWidget);
        mngrLayout->setSpacing(6);
        mngrLayout->setContentsMargins(11, 11, 11, 11);
        mngrLayout->setObjectName(QString::fromUtf8("mngrLayout"));
        mngrLayout->setContentsMargins(0, 0, 0, 0);
        sqlLBL = new QLabel(verticalLayoutWidget);
        sqlLBL->setObjectName(QString::fromUtf8("sqlLBL"));

        mngrLayout->addWidget(sqlLBL);

        mngrLW = new QListWidget(verticalLayoutWidget);
        mngrLW->setObjectName(QString::fromUtf8("mngrLW"));

        mngrLayout->addWidget(mngrLW);

        sqlLE = new QLineEdit(verticalLayoutWidget);
        sqlLE->setObjectName(QString::fromUtf8("sqlLE"));

        mngrLayout->addWidget(sqlLE);

        mngrBTN = new QPushButton(verticalLayoutWidget);
        mngrBTN->setObjectName(QString::fromUtf8("mngrBTN"));

        mngrLayout->addWidget(mngrBTN);

        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 390, 341, 141));
        buttonLayout = new QHBoxLayout(horizontalLayoutWidget);
        buttonLayout->setSpacing(6);
        buttonLayout->setContentsMargins(11, 11, 11, 11);
        buttonLayout->setObjectName(QString::fromUtf8("buttonLayout"));
        buttonLayout->setContentsMargins(0, 0, 0, 0);
        gateReadBTN = new QPushButton(horizontalLayoutWidget);
        gateReadBTN->setObjectName(QString::fromUtf8("gateReadBTN"));

        buttonLayout->addWidget(gateReadBTN);

        gateWriteBTN = new QPushButton(horizontalLayoutWidget);
        gateWriteBTN->setObjectName(QString::fromUtf8("gateWriteBTN"));

        buttonLayout->addWidget(gateWriteBTN);

        rfidReadBTN = new QPushButton(horizontalLayoutWidget);
        rfidReadBTN->setObjectName(QString::fromUtf8("rfidReadBTN"));

        buttonLayout->addWidget(rfidReadBTN);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Carpark Management System", nullptr));
        consoleLBL->setText(QApplication::translate("MainWindow", "Console:", nullptr));
        pInfoLBL->setText(QApplication::translate("MainWindow", " Pers\303\266nliche Informationen:", nullptr));
#ifndef QT_NO_TOOLTIP
        nameLE->setToolTip(QApplication::translate("MainWindow", "Geben Sie den Namen des Mitarbeiters an.", nullptr));
#endif // QT_NO_TOOLTIP
        nameLE->setText(QString());
        nameLE->setPlaceholderText(QApplication::translate("MainWindow", "Name..", nullptr));
#ifndef QT_NO_TOOLTIP
        surnameLE->setToolTip(QApplication::translate("MainWindow", "Geben Sie den Vornamen des Mitarbeiters ein.", nullptr));
#endif // QT_NO_TOOLTIP
        surnameLE->setText(QString());
        surnameLE->setPlaceholderText(QApplication::translate("MainWindow", "Vorname..", nullptr));
        fInfoLBL->setText(QApplication::translate("MainWindow", " Firmen Informationen:", nullptr));
#ifndef QT_NO_TOOLTIP
        cnameLE->setToolTip(QApplication::translate("MainWindow", "Geben Sie den Firmennamen der Firma ein, in welcher der Mitarbeiter arbeitet.", nullptr));
#endif // QT_NO_TOOLTIP
        cnameLE->setPlaceholderText(QApplication::translate("MainWindow", "Firmenname..", nullptr));
#ifndef QT_NO_TOOLTIP
        cadressLE->setToolTip(QApplication::translate("MainWindow", "Geben Sie die Adresse der Firma an, in welcher der Mitarbeiter arbeitet.", nullptr));
#endif // QT_NO_TOOLTIP
        cadressLE->setPlaceholderText(QApplication::translate("MainWindow", "Adresse..", nullptr));
#ifndef QT_NO_TOOLTIP
        addPBTN->setToolTip(QApplication::translate("MainWindow", "Indem Sie auf \"Hinzuf\303\274gen\" klicken, legen Sie f\303\274r den Mitarbeiter einen neuen Eintrag in der Datenbank an.", nullptr));
#endif // QT_NO_TOOLTIP
        addPBTN->setText(QApplication::translate("MainWindow", "Hinzuf\303\274gen", nullptr));
        sqlLBL->setText(QApplication::translate("MainWindow", "SQL-Manager:", nullptr));
#ifndef QT_NO_TOOLTIP
        sqlLE->setToolTip(QApplication::translate("MainWindow", "Hier k\303\266nnen Sie SQL-Befehle eingeben, um die Datenbank nachtr\303\244glich zu bearbeiten.", nullptr));
#endif // QT_NO_TOOLTIP
        sqlLE->setPlaceholderText(QApplication::translate("MainWindow", "Input SQL..", nullptr));
#ifndef QT_NO_TOOLTIP
        mngrBTN->setToolTip(QApplication::translate("MainWindow", "Klicken Sie auf \"Senden\" um den SQL-Befehl auszuf\303\274hren.", nullptr));
#endif // QT_NO_TOOLTIP
        mngrBTN->setText(QApplication::translate("MainWindow", "Senden", nullptr));
#ifndef QT_NO_TOOLTIP
        gateReadBTN->setToolTip(QApplication::translate("MainWindow", "Klicken Sie auf \"Read Gate\", um die ReaderID anzeigen zu lassen.", nullptr));
#endif // QT_NO_TOOLTIP
        gateReadBTN->setText(QApplication::translate("MainWindow", "Read Gate..", nullptr));
#ifndef QT_NO_TOOLTIP
        gateWriteBTN->setToolTip(QApplication::translate("MainWindow", "Wenn Sie auf \"Write Gate\" klicken, wird die ID dem Programm \303\274bermittelt.", nullptr));
#endif // QT_NO_TOOLTIP
        gateWriteBTN->setText(QApplication::translate("MainWindow", "Write Gate..", nullptr));
#ifndef QT_NO_TOOLTIP
        rfidReadBTN->setToolTip(QApplication::translate("MainWindow", "Klicken Sie auf \"Read RFID\" um die ID der Karte zu lesen, die auf das Kartenleseger\303\244t gelegt wurde.", nullptr));
#endif // QT_NO_TOOLTIP
        rfidReadBTN->setText(QApplication::translate("MainWindow", "Read RFID..", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
