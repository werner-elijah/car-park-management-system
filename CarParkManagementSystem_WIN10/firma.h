#ifndef FIRMA_H
#define FIRMA_H

#include <QString>

class firma
{
private:
    QString g_cName, g_address;

public:
    firma();
    void setCName(QString cname);
    void setAddress(QString adress);
    QString getCName();
    QString getAddress();
};

#endif // FIRMA_H
