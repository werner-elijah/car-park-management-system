#ifndef MITARBEITER_H
#define MITARBEITER_H

#include <QString>
#include "serialinterface.h"

class person
{
private:

QString g_vName, g_nName, g_id;

public:
    person();
    void setVName(QString vname);
    void setNName(QString nname);
    void setID(SerialInterface *si);
    QString getVName();
    QString getNName();
    QString getID();
};

#endif // MITARBEITER_H
