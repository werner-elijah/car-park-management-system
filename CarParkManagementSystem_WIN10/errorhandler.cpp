#include "errorhandler.h"

errorhandler::errorhandler(){}

void errorhandler::setLastDatabaseError(QString error)
{
    this->g_dbError = error;
}

void errorhandler::setLastQueryError(QString error)
{
    this->g_qError = error;
}

QString errorhandler::getLastDatabaseError()
{
    return g_dbError;
}

QString errorhandler::getLastQueryError()
{
    return g_qError;
}
