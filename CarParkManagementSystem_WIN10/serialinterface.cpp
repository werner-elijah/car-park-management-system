#include "serialinterface.h"

SerialInterface::SerialInterface()
{
    Gate = new QSerialPort;
    Reader = new QSerialPort;
    ReaderIsOnline=false;
    GateSystemIsOnline=false;
}

void SerialInterface::showPorts()
{
    qDebug() << "Number of available ports: " << QSerialPortInfo::availablePorts().length();

    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
        qDebug() << "Port Name:  " << serialPortInfo.portName();
        qDebug() << "Has vendor ID: " << serialPortInfo.hasVendorIdentifier();

        if(serialPortInfo.hasVendorIdentifier())
        {
            qDebug() << "Vendor ID: " << serialPortInfo.vendorIdentifier();
        }

        qDebug() << "Has Product ID: " << serialPortInfo.hasProductIdentifier();

        if(serialPortInfo.hasProductIdentifier())
        {
            qDebug() << "Product ID: " << serialPortInfo.productIdentifier();
        }
    }
}

void SerialInterface::ReadReader()
{
    if(Reader->isReadable())
    {
        QString ID=NULL;

        char id[13];
        Reader->readLine(id, sizeof (id));

        for(int i=0;i<12;i++)
        {
            ID.push_back(QChar(id[i]));
        }
        setReaderID(ID);
    }
}

void SerialInterface::StartReadWriteGate()
{
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
        if(serialPortInfo.hasVendorIdentifier()&&serialPortInfo.hasProductIdentifier())
        {
            if(serialPortInfo.vendorIdentifier()==gate_hersteller_id)
            {
                if(serialPortInfo.productIdentifier()==gate_produkt_id)
                {
                    GateSystemIsOnline=true;
                    GatePortName=serialPortInfo.portName();
                }
            }
        }
    }

    if(GateSystemIsOnline==true)
    {
        Gate->setPortName(GatePortName);
        Gate->open(QSerialPort::ReadWrite);
        Gate->setBaudRate(QSerialPort::Baud115200);
        Gate->setDataBits(QSerialPort::Data8);
        Gate->setParity(QSerialPort::NoParity);
        Gate->setStopBits(QSerialPort::OneAndHalfStop);
        Gate->setFlowControl(QSerialPort::NoFlowControl);
    }
}

void SerialInterface::StartReadReader(){

    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
        if(serialPortInfo.hasVendorIdentifier()&&serialPortInfo.hasProductIdentifier())
        {
            if(serialPortInfo.vendorIdentifier()==reader_hersteller_id)
            {
                if(serialPortInfo.productIdentifier()==reader_produkt_id)
                {
                    ReaderIsOnline=true;
                    ReaderPortName=serialPortInfo.portName();
                }
            }
        }
    }

    if(ReaderIsOnline==true)
    {
        Reader->setPortName(ReaderPortName);
        Reader->open(QSerialPort::ReadOnly);
        Reader->setBaudRate(QSerialPort::Baud9600);
        Reader->setDataBits(QSerialPort::Data8);
        Reader->setParity(QSerialPort::NoParity);
        Reader->setStopBits(QSerialPort::OneAndHalfStop);
        Reader->setFlowControl(QSerialPort::NoFlowControl);
    }
}

void SerialInterface::setReaderID(QString id)
{
    this->ReaderID=id;
}

QString SerialInterface::getReaderID()
{
    return(this->ReaderID);
}

void SerialInterface::setGateID(QString id)
{
    this->ID = id;
}

QString SerialInterface::getGateID()
{
    return(this->ID);
}

void SerialInterface::ReadGate()
{
    if(Gate->isReadable())
    {
        QString ID=NULL;

        char id[13];
        Gate->readLine(id, sizeof (id));

        for(int i=0;i<12;i++)
        {
            ID.push_back(QChar(id[i]));
        }
        setGateID(ID);
    }
}



void SerialInterface::setCheckedID(bool id)
{
    if(id==true){this->CheckedID = '1';}
    if(id==false){this->CheckedID = '0';}
}

QString SerialInterface::getCheckedID()
{
    return(this->CheckedID);
}

bool SerialInterface::WriteGate()
{
    if(Gate->isWritable())
    {
        Gate->write(getCheckedID().toStdString().c_str());
        return true;
    }
    else
    {
        return false;
    }
}

SerialInterface::~SerialInterface()
{
    if(Gate->isOpen())
    {
        Gate->close();
        Reader->close();
        delete Gate;
        delete Reader;
    }
}
