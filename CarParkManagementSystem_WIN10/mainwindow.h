#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QDebug>
#include <iostream>
#include <chrono>
#include <ctime>
#include "prefixhandler.h"
#include "dbmanager.h"
#include "person.h"
#include "firma.h"
#include "serialinterface.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_mngrBTN_clicked();
    void on_addPBTN_clicked();

    void on_gateReadBTN_clicked();

    void on_gateWriteBTN_clicked();

    void on_rfidReadBTN_clicked();

private:
    Ui::MainWindow *ui;
    prefixhandler prefix;
    SerialInterface *si = new SerialInterface;
    dbmanager db;
    errorhandler *error = new errorhandler;
    person *p = new person;
    firma *f = new firma;

};

#endif // MAINWINDOW_H
