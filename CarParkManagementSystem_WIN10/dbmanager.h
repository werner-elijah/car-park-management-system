#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QMainWindow>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QString>
#include "person.h"
#include "firma.h"
#include "prefixhandler.h"
#include "serialinterface.h"
#include "errorhandler.h"

class dbmanager
{
public:
    dbmanager();
    bool start(QString host, int port);
    bool start(QString host);
    bool stop();
    bool sendData(person *p, firma *f);
    bool sendData(QString cmd);
    QString getData(QString data);
    void setCheckedID(bool);
    bool getCheckedID();
    bool CheckID(SerialInterface *si);
    void setErrHandler(errorhandler*);

private:
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    errorhandler *error;
    prefixhandler prefix;
    QString global_driver = "QSQLITE";
    QString ID;
    bool CheckedID;
    bool IDCorrect;
};

#endif // DBMANAGER_H
