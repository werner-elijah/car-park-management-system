#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    int hour = QTime::currentTime().hour();

    setFixedSize(692, 538);

    db.setErrHandler(error);

    si->StartReadReader();
    si->StartReadWriteGate();

    ui->setupUi(this);

    ui->consoleLW->addItem("Console ist bereit..!");
    ui->mngrLW->addItem("SQL-Manager ist bereit..!");
    qDebug() << prefix.consolePrefix + "Console ist bereit..!";
    qDebug() << prefix.consolePrefix + "SQL-Manager ist bereit..!";

    if (hour < 12){ ui->consoleLW->addItem("Guten Morgen!"); }
    else if (hour >= 12 && hour < 18){ ui->consoleLW->addItem("Guten Mittag!"); }
    else if (hour >= 18 && hour <= 23){ ui->consoleLW->addItem("Guten Abend!"); }
}

MainWindow::~MainWindow()
{
    si->~SerialInterface();
    delete ui;
}

void MainWindow::on_mngrBTN_clicked()
{
    // SQL Command
    QString cmd = ui->sqlLE->text();

    // Schlüsselwörter
    QString b_send = "INSERT INTO";
    QString b_receive = "SELECT";
    QString b_delete = "DELETE";
    QString b_clear = "TRUNCATE";
    QString b_order = "ORDER BY";

    QString s_send = "insert into";
    QString s_receive = "select";
    QString s_delete = "delete";
    QString s_clear = "truncate";
    QString s_order = "order by";

    bool start = db.start("debug/CarparkDB.db");

    if (!ui->sqlLE->text().isEmpty())
    {
        if (start == true && (cmd.contains(s_send) || cmd.contains(b_send)))
        {
            bool sent = db.sendData(cmd);

            if (sent == true)
            {
                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem(prefix.qyrPrefix + "Daten wurden gesendet!");
                qDebug() << "Debug: Data sent!";
            }
            else
            {
                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem(prefix.qyrPrefix + "Daten konnten nicht gesendet werden!");
                ui->mngrLW->addItem(prefix.errPrefix + error->getLastQueryError());
                qDebug() << "Debug: Data not sent!";
            }
        }
        else if (start == true && (cmd.contains(s_receive) || cmd.contains(b_receive)))
        {
            if (cmd.contains(s_order) || cmd.contains(b_order))
            {
                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem(prefix.errPrefix + "ORDER BY darf nicht ausgeführt werden!");
            }
            else
            {
                QString callback = db.getData(cmd);

                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem("Debug: Data received!");
                qDebug() << "Debug: Data received!";
                ui->mngrLW->addItem(prefix.qyrPrefix + callback);
            }
        }
        else if (start == true && (cmd.contains(s_delete) || cmd.contains(b_delete)))
        {
            bool sent = db.sendData(cmd);

            if (sent == true)
            {
                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem(prefix.dbPrefix + "Die Einträge wurden erfolgreich gelöscht.");
                qDebug() << prefix.debugPrefix + "The data was deleted!";
            }
            else
            {
                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem(prefix.dbPrefix + "Die Einträge konnten nicht gelöscht werden.");
                ui->mngrLW->addItem(prefix.errPrefix + error->getLastQueryError());
                qDebug() << prefix.debugPrefix + "The data was not deleted!";
            }
        }
        else if (start == true && (cmd.contains(s_clear) || cmd.contains(b_clear)))
        {
            bool sent = db.sendData(cmd);

            if (sent == true)
            {
                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem(prefix.dbPrefix + "Der Table wurde erfolgreich gelöscht!");
                qDebug() << prefix.debugPrefix + "Deleted Table!";
            }
            else
            {
                ui->mngrLW->addItem(prefix.insertPrefix + cmd);
                ui->mngrLW->addItem(prefix.errPrefix + error->getLastQueryError());
                qDebug() << prefix.debugPrefix + "Could not delete Table!";
            }
        }
        else
        {
            ui->mngrLW->addItem(prefix.insertPrefix + cmd);
            ui->mngrLW->addItem(prefix.errPrefix + "Sie haben eine ungültige Eingabe getätigt!");
            qDebug() << prefix.debugPrefix + "Wrong expression!";
        }
    }
    else
    {
        ui->mngrLW->addItem(prefix.errPrefix + "Das Eingabefeld darf nicht leer sein..!");
        qDebug() << prefix.debugPrefix + "Input in Console-LineEdit is NULL!";
    }

    db.stop();

}

void MainWindow::on_addPBTN_clicked()
{
    bool start = db.start("debug/CarparkDB.db");

    if(start == true)
    {
        p->setID(si);
        p->setNName(ui->nameLE->text());
        p->setVName(ui->surnameLE->text());

        f->setCName(ui->cnameLE->text());
        f->setAddress(ui->cadressLE->text());

        if (ui->nameLE->text().isEmpty() || ui->surnameLE->text().isEmpty() || ui->cnameLE->text().isEmpty() || ui->cadressLE->text().isEmpty())
        {
            ui->consoleLW->addItem(prefix.errPrefix + "Die Eingabefelder dürfen nicht leer sein..!");
            qDebug() << prefix.debugPrefix + "Input in SQL-LineEdit is NULL!";
        }
        else
        {
            bool sent = db.sendData(p, f);

            if (sent == true)
            {
                ui->consoleLW->addItem(prefix.consolePrefix + "Daten wurden gesendet..");
                ui->nameLE->clear();
                ui->surnameLE->clear();
                ui->cnameLE->clear();
                ui->cadressLE->clear();
            }
            else
            {
                ui->consoleLW->addItem(prefix.errPrefix + "Daten konnten nicht gesendet werden!");
                ui->consoleLW->addItem(prefix.errPrefix + error->getLastQueryError());
            }
        }

        db.stop();

    }
}


void MainWindow::on_gateReadBTN_clicked()
{
    si->ReadGate();
    db.start("debug/CarparkDB.db");
    db.CheckID(si);
    ui->consoleLW->addItem(prefix.readPrefix + si->getGateID());
    db.stop();
}

void MainWindow::on_gateWriteBTN_clicked()
{

    bool written = si->WriteGate();

    if (written == true)
    {
        ui->consoleLW->addItem(prefix.consolePrefix + "Daten wurden übertragen..!");
    }
    else
    {
        ui->consoleLW->addItem(prefix.errPrefix + "Daten konnten nicht übertragen werden!");
    }
}

void MainWindow::on_rfidReadBTN_clicked()
{
    si->ReadReader();
    ui->consoleLW->addItem(prefix.rfidPrefix + si->getReaderID());
}
