#ifndef PREFIXHANDLER_H
#define PREFIXHANDLER_H

#include <QString>

class prefixhandler
{
public:
    prefixhandler();
    QString dbPrefix = "Datenbank: ";
    QString qyrPrefix = "Query: ";
    QString errPrefix = "Error: ";
    QString readPrefix = "Reader: ";
    QString rfidPrefix = "RFID: ";
    QString debugPrefix = "Debug: ";
    QString insertPrefix = "Eingabe: ";
    QString consolePrefix = "Console: ";
};

#endif // PREFIXHANDLER_H
