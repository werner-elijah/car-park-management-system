#include "dbmanager.h"

dbmanager::dbmanager(){}

bool dbmanager::start(QString host, int port)
{
    db.setPort(port);
    db.setDatabaseName(host);


    if (db.open())
    {
        QSqlDatabase database = QSqlDatabase::database();
        QSqlQuery query1(database);
        QSqlQuery query2(database);

        qDebug() << prefix.dbPrefix + "Datenbank geöffnet...";
        query1.prepare("CREATE TABLE IF NOT EXISTS person (id TEXT PRIMARY KEY, name TEXT, vorname TEXT)");

        if(query1.exec())
        {
            query1.finish();
            query2.prepare("CREATE TABLE IF NOT EXISTS firma (firmenname TEXT PRIMARY KEY, strasse TEXT)");

            if(query2.exec())
            {
                query2.finish();
                return true;
            }
        }
        else
        {
            qDebug() << prefix.errPrefix + "Query konnte nicht ausgeführt werden..!";
            qDebug() << "Error: " << query1.lastError();
            error->setLastQueryError(query1.lastError().text());
        }
    }
    else
    {
        qDebug() << prefix.errPrefix + "Datenbank konnte nicht geöffnet werden..!";
        qDebug() << "Error: " << db.lastError();
        error->setLastDatabaseError(db.lastError().text());
        return false;
    }
}

bool dbmanager::start(QString host)
{
    db.setDatabaseName(host);      // "C:/DATABASE_PATH/Database.db"

    // Falls man Auth aktiviert hat
    // db.setUserName("");
    // db.setPassword("");

    bool isOpen = db.open();

    if (isOpen)
    {
        QSqlDatabase database = QSqlDatabase::database();
        QSqlQuery query1(database);
        QSqlQuery query2(database);

        qDebug() << prefix.dbPrefix + "Datenbank geöffnet...";
        query1.prepare("CREATE TABLE IF NOT EXISTS person (id TEXT PRIMARY KEY, name TEXT, vorname TEXT)");

        if(query1.exec())
        {
            query1.finish();
            query2.prepare("CREATE TABLE IF NOT EXISTS firma (firmenname TEXT PRIMARY KEY, strasse TEXT)");

            if(query2.exec())
            {
                query2.finish();
                return true;
            }
        }
        else
        {
            qDebug() << prefix.errPrefix + "Query konnte nicht ausgeführt werden..!";
            qDebug() << "Error: " << query1.lastError();
            error->setLastQueryError(query1.lastError().text());
            return false;
        }
    }
    else
    {
        qDebug() << prefix.errPrefix + "Datenbank konnte nicht geöffnet werden..!";
        qDebug() << "Error: " << db.lastError();
        error->setLastDatabaseError(db.lastError().text());
        return false;
    }
}

bool dbmanager::stop()
{
    db.close();
    db.removeDatabase(global_driver);
    qDebug() << prefix.dbPrefix + "Datenbankverbindung geschlossen...";
    return true;
}

bool dbmanager::sendData(person *p, firma *f)
{
    QSqlDatabase database = QSqlDatabase::database();
    QSqlQuery query(database);
    QSqlQuery query2(database);

    // For testing:
    // INSERT INTO person (id, name, vorname) VALUES ('test1', 'test2', 'test3')

    query.prepare("INSERT INTO person (id, name, vorname) VALUES (:id, :name, :vorname)");
    query.bindValue(":id", p->getID());
    query.bindValue(":name", p->getNName());
    query.bindValue(":vorname", p->getVName());

    if (query.exec())
    {
        query.finish();
        query2.prepare("INSERT INTO firma (firmenname, strasse) VALUES (:firmenname, :strasse)");
        query2.bindValue(":firmenname", f->getCName());
        query2.bindValue(":strasse", f->getAddress());
        qDebug() << prefix.dbPrefix + "Daten (Person) wurden übermittelt...";

        if (query2.exec())
        {
            qDebug() << prefix.dbPrefix + "Daten (Firma) wurden übermittelt...";
            qDebug() << "Error: " << query2.lastError();
            error->setLastQueryError(query2.lastError().text());
            query2.finish();
            return true;
        }
        else
        {
            qDebug() << prefix.errPrefix + "Daten (Firma) wurden nicht übermittelt...";
        }
    }
    else
    {
        qDebug() << prefix.errPrefix + "Daten (Person) wurden nicht übermittelt..!";
        qDebug() << "Error: " << query.lastError();
        error->setLastQueryError(query.lastError().text());
        query.finish();
        return false;
    }
}

bool dbmanager::sendData(QString cmd)
{
    QSqlDatabase database = QSqlDatabase::database();
    QSqlQuery query(database);

    query.prepare(cmd);

    if(query.exec())
    {
        return true;
    }
    else
    {
        error->setLastQueryError(query.lastError().text());
        return false;
    }
}

// Anpassen
QString dbmanager::getData(QString data)
{
    QString dataInside;
    QSqlDatabase database = QSqlDatabase::database();
    QSqlQuery query(database);
    int tick = 0;

    if (query.exec(data))
    {
        while(query.next())
        {
            dataInside = query.value(tick).toString();
            qDebug() << prefix.qyrPrefix + "Daten von Datenbank erhalten..";
            query.finish();
            tick++;
            return dataInside;
        }
    }
    else
    {
        qDebug() << prefix.errPrefix + "Daten konnten nicht abgerufen werden..!";
        error->setLastQueryError(query.lastError().text());
    }
}

void dbmanager::setCheckedID(bool cID)
{
    this->CheckedID=cID;
}

bool  dbmanager::getCheckedID()
{
    return(CheckedID);
}

bool dbmanager::CheckID(SerialInterface *si)
{
    QSqlDatabase database = QSqlDatabase::database();
    QSqlQuery qry(database);

    ID=si->getGateID();
    qDebug()<<si->getGateID();

    if(!db.isOpen())
    {
        qDebug() << prefix.errPrefix + "Datenbank konnte nicht geöffnet werden..!";
        return false;
    }

    if(qry.exec("SELECT * FROM person WHERE id=\'"+ ID +"\'"))
    {
        int count=0;
        this->IDCorrect = false;

        while(qry.next())
        {
            count++;
        }

        if(count==1)
        {
            qDebug()<<"ID richtig";
            this->IDCorrect = true;
            si->setCheckedID(IDCorrect);
            return true;
        }
        else
        {
            qDebug()<<"ID falsch";
            this->IDCorrect = false;
            si->setCheckedID(IDCorrect);
            return false;
        }
    }
}

void dbmanager::setErrHandler(errorhandler *err)
{
    this->error = err;
}
