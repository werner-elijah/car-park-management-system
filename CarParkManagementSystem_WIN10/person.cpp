#include "person.h"


person::person(){}

void person::setVName(QString vname)
{
    this->g_vName = vname;
}

void person::setNName(QString nname)
{
    this->g_nName = nname;
}

void person::setID(SerialInterface *si)
{
    this->g_id =si->getReaderID();
}

QString person::getVName()
{
    return(g_vName);
}

QString person::getNName()
{
    return(g_nName);
}

QString person::getID()
{
    return(g_id);
}
