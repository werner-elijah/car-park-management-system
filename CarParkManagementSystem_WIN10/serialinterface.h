#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include <QString>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QIODevice>
#include <QDebug>

class SerialInterface
{
private:
    QSerialPort *Gate, *Reader;
    static const quint16 gate_hersteller_id =  6790;
    static const quint16 gate_produkt_id = 29987;
    static const quint16 reader_hersteller_id =  10755;
    static const quint16 reader_produkt_id = 67;
    bool GateSystemIsOnline=false;
    bool ReaderIsOnline=false;
    QString CheckedID;
    QString GatePortName, ReaderPortName;
    QString ID, ReaderID;

public:
    SerialInterface();
    ~SerialInterface();
    void showPorts(); // Zeigt verfügbare Ports an
    void StartReadWriteGate(); // Start Reader
    void StartReadReader();
    void ReadGate();
    bool WriteGate();
    void setGateID(QString);
    QString getGateID();
    void setCheckedID(bool);
    QString getCheckedID();
    void ReadReader();
    void setReaderID(QString);
    QString getReaderID();
};

#endif // SERIALINTERFACE_H
