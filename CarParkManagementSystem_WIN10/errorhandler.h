#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H

#include <QString>
#include <QDebug>

class errorhandler
{
public:
    errorhandler();
    void setLastDatabaseError(QString dberror);
    void setLastQueryError(QString qryerror);
    QString getLastDatabaseError();
    QString getLastQueryError();

    QString g_dbError;
    QString g_qError;
};

#endif // ERRORHANDLER_H
