int Sensor = 12;  // This is our input pin
int DetectedObject = HIGH;  // HIGH MEANS NO OBSTACLE

void setup() {
  pinMode(Sensor, INPUT);
  Serial.begin(9600);  
}

void loop() {
  DetectedObject = digitalRead(Sensor);
  if (DetectedObject == LOW)
  {
    Serial.println("Fahrweg blockiert");
  }
  else
  {
    Serial.println("Fahrweg frei");
  }
  delay(200);
}
