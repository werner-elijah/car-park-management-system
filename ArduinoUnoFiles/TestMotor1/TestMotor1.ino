
#include <Wire.h>
#include <EVShield.h>
#include <EVs_NXTTouch.h>



EVShield          evshield(0x34,0x36);
EVs_NXTTouch GateButtonC, GateButtonO;
bool stat=false;

void setup()
{
    Serial.begin(115200);       
    delay(2000);                

    



    evshield.init( SH_HardwareI2C );
    GateButtonO.init(&evshield,SH_BBS1);
    GateButtonC.init(&evshield,SH_BBS2);


    while (!evshield.getButtonState(BTN_GO)) {
        if (millis() % 1000 < 3) {
            Serial.println("Press GO button to continue");
        }
    }
        
    

    
    
    evshield.bank_a.motorReset();
    
    }

    
void  loop()                          
{
    delay(50);
    GateCycel();
    delay(50);

}




void GateCycel(){

//Öffnen

while(!evshield.getButtonState(BTN_RIGHT)==true);
if(evshield.getButtonState(BTN_RIGHT)==true){
evshield.bank_a.motorRunUnlimited(SH_Motor_1, SH_Direction_Reverse,SH_Speed_Medium);
}
                     
while(!GateButtonO.isPressed());
  if(GateButtonO.isPressed()){
evshield.bank_a.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);
Serial.println("Motor Stopt");
}

//Schließen

while(!evshield.getButtonState(BTN_LEFT)==true);
if(evshield.getButtonState(BTN_LEFT)==true){
evshield.bank_a.motorRunUnlimited(SH_Motor_1, SH_Direction_Forward,SH_Speed_Medium);

}
while(!GateButtonC.isPressed());
  if(GateButtonC.isPressed()){
evshield.bank_a.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);
Serial.println("Motor Stopt");

  }

}
  
