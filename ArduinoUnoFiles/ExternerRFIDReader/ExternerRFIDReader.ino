#include <SPI.h>
#include <MFRC522.h>
//Vordefinierte In_PINs: MOSI 11, MISO 12, SCK 13; 
#define RST_PIN         9          
#define SS_1_PIN        8         
#define NR_OF_READERS   1

byte ssPins[] = {SS_1_PIN};     
    
MFRC522 mfrc522[NR_OF_READERS];  

void setup() {
  Serial.begin(9600);
  SPI.begin();                                                  
     for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    mfrc522[reader].PCD_Init(ssPins[reader], RST_PIN);
   // mfrc522[reader].PCD_DumpVersionToSerial();
     }   
}

void loop() {
CardScan();
}


void CardScan(){

for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
if (mfrc522[reader].PICC_IsNewCardPresent() && mfrc522[reader].PICC_ReadCardSerial()) {
      MFRC522::PICC_Type piccType = mfrc522[reader].PICC_GetType(mfrc522[reader].uid.sak);
      mfrc522[reader].PICC_HaltA();    
      mfrc522[reader].PCD_StopCrypto1();
     // Serial.println("Card present");
    } 
  } 
 if(mfrc522[0].PICC_IsNewCardPresent() && mfrc522[0].PICC_ReadCardSerial()) {
     //Serial.println("Card present");
     for(int i=0; i<4;i++){
     Serial.print(mfrc522[0].uid.uidByte[i]);
     }
     Serial.println();
     delay(250);
     for(int i=0;i<4;i++){
     mfrc522[0].uid.uidByte[i]=NULL;
     }

}
}
