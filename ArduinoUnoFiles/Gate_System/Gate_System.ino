#include <Wire.h>
#include <EVShield.h>
#include <EVs_NXTTouch.h>

//******************EVShield***********************************
EVShield     evshield(0x34,0x36);
EVs_NXTTouch InGateButtonC;
EVs_NXTTouch InGateButtonO;
EVs_NXTTouch OutGateButtonC;
EVs_NXTTouch OutGateButtonO;

//******************Avoid_Sensor***********************************
int InSensor = 2; 
int OutSensor = 3; 
int DetectedObjectIn = HIGH; 
int DetectedObjectOut = HIGH;   

//******************RFID_Reader***********************************
#include <SPI.h>
#include <MFRC522.h>
//Vordefinierte In_PINs: MOSI 11, MISO 12, SCK 13; 
#define RST_PIN         9          
#define SS_1_PIN        8         
#define SS_2_PIN        10  
#define NR_OF_READERS   2

byte ssPins[] = {SS_1_PIN, SS_2_PIN};     
    
MFRC522 mfrc522[NR_OF_READERS];  
//1.FA 30 BB 25
// 2.09 0E B1 A3
// 3.69 A1 CD A2

//******************Variabel_Deklaration***********************************
    enum Gate{
    IDLESTATE,
    CHECKSTATE,
    GATESTATE,    
              };

enum Gate InGate, OutGate;              
bool InID = false, OutID = false;
int Send=0;
void setup()
{   
    //******************Bitrate***********************************
    Serial.begin(115200);    
    

    //******************InititalisierungDerMindstormsTechnik***********************************
    evshield.init( SH_HardwareI2C );
    InGateButtonO.init(&evshield,SH_BAS1);
    InGateButtonC.init(&evshield,SH_BAS2);
    OutGateButtonO.init(&evshield,SH_BBS1);
    OutGateButtonC.init(&evshield,SH_BBS2);
    //******************DeklarationDesSensorPINs***********************************
    pinMode(InSensor, INPUT);
    pinMode(OutSensor, INPUT);          
  
    //******************InitialisierungRFID_Scanners***********************************
    SPI.begin();                                                  
     for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    mfrc522[reader].PCD_Init(ssPins[reader], RST_PIN);
   // mfrc522[reader].PCD_DumpVersionToSerial();
     }                                       
    
    
    //******************RestlicheVorbereitungVonTechnik&Code***********************************

    evshield.bank_a.motorReset();
    
    //******************ProgrammStart***********************************
    byte buffer[2];
    bool Start=false;
    delay(2000);
    while (!evshield.getButtonState(BTN_GO)) {
        if (millis() % 1000 < 3) {
         //   Serial.println("Press GO button to continue");
        }
    
    }
      //Serial.println(F("-- System gestartet --"));   


      //******************Schranken werden geschlossen***********************************

    evshield.bank_a.motorRunUnlimited(SH_Motor_1, SH_Direction_Forward,SH_Speed_Slow);
    evshield.bank_b.motorRunUnlimited(SH_Motor_1, SH_Direction_Forward,SH_Speed_Slow);

    while(InGateButtonC.isPressed()==LOW||OutGateButtonC.isPressed()==LOW){delay(5);/*Serial.println("Setup started")*/;}
        if(InGateButtonC.isPressed()||OutGateButtonC.isPressed()){
            evshield.bank_a.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);
            evshield.bank_b.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);
         //   Serial.println("Setup finished");
            
         }
         delay(750);
         evshield.bank_a.motorReset();
         evshield.bank_b.motorReset();

}
void setInID(char iid){

switch(iid){

case'0': InID=false; break;
case'1': InID=true; break;
 
}

}
bool getInID(){

return(InID);

}
void setOutID(char oid){
switch(oid){  
case'0': OutID=false; break;
case'1': OutID=true; break;
}
}
bool getOutID(){

return(OutID);

}

void  loop()                          
{   
    
    delay(5);
    GateCycle();
    delay(5);
    //CardScan();
    //Serial.println(CardScan());
    delay(5);
    //InObjectScan();
    //OutObjectScan();

}


//___________________________________________________________________________________________________________________________________________________________________________________________

void GateCycle(){
  
    DetectedObjectIn = digitalRead(InSensor);
    DetectedObjectOut = digitalRead(OutSensor);

  
switch(InGate){

case IDLESTATE:   
//Serial.println("IdleState");
DetectedObjectIn = digitalRead(InSensor);
evshield.bank_a.motorReset();
if(DetectedObjectIn==LOW){
  
  InGate = CHECKSTATE;
  
}
break;
case CHECKSTATE:
// Serial.println("CheckState");
if(DetectedObjectIn==HIGH){
  
  InGate = IDLESTATE;
  
}
  bool ID;
  CardScan();
  if(getInID()==true){
   ID=true; 
  // Serial.print("ID: ");Serial.println(ID);
  
  if(ID==true){ 
  

  InGate = GATESTATE;
  
  }}
break;
case GATESTATE:
 // Serial.println("GateState");
  setInID('0');
        //Öffnen
        
        evshield.bank_a.motorRunUnlimited(SH_Motor_1, SH_Direction_Reverse,SH_Speed_Slow);
        
        
        while(InGateButtonO.isPressed()==LOW){delay(5);/*Serial.println("Schleife 1")*/;}
        if(InGateButtonO.isPressed()){
            evshield.bank_a.motorReset();
            evshield.bank_a.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);
           // Serial.println("Motor Stopt 1");
        
                                    }       
        //Schließen
        DetectedObjectIn = digitalRead(InSensor);
        //Serial.println("Schließt");
        delay(5);
        if(DetectedObjectIn==HIGH){
        delay(5000);
        
        evshield.bank_a.motorRunUnlimited(SH_Motor_1, SH_Direction_Forward,SH_Speed_Slow);
                    
        while(InGateButtonC.isPressed()==LOW){delay(5);/*Serial.println("Schleife 2")*/;}
        if(InGateButtonC.isPressed()){
          evshield.bank_a.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);delay(1000);
            evshield.bank_a.motorReset();
            // Serial.println("Motor Stopt 2");                                
         }            
                      ID=false;
                      InGate=IDLESTATE; break; 
                                 }
         
                                 

//default: InGate = IDLESTATE; 
}
switch(OutGate){

case IDLESTATE:   
//Serial.println("IdleState");
DetectedObjectOut = digitalRead(OutSensor);
evshield.bank_b.motorReset();
if(DetectedObjectOut==LOW){

  OutGate = CHECKSTATE;
  
}
break;
case CHECKSTATE:
  // Serial.println("CheckState");
  if(DetectedObjectOut==HIGH){

  OutGate = IDLESTATE;
  
}
  bool IDOUT;
  CardScan();
  if(getOutID()==true){
   IDOUT=true; 
  // Serial.print("ID: ");Serial.println(ID);
  
  if(IDOUT==true){ 
  

  OutGate = GATESTATE;
  
  }}
break;
case GATESTATE:
// Serial.println("GateState");
 setOutID('0');
        //Öffnen

        evshield.bank_b.motorRunUnlimited(SH_Motor_1, SH_Direction_Reverse,SH_Speed_Slow);
        
        
        while(OutGateButtonO.isPressed()==LOW){delay(5);/*Serial.println("Schleife 1")*/;}
        if(OutGateButtonO.isPressed()){
            evshield.bank_b.motorReset();
            evshield.bank_b.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);
            //Serial.println("Motor Stopt 1");
        
                                    }       
        //Schließen
        DetectedObjectOut = digitalRead(OutSensor);
       // Serial.println("Schließt");

        if(DetectedObjectOut==HIGH){
        delay(5000);
        evshield.bank_b.motorRunUnlimited(SH_Motor_1, SH_Direction_Forward,SH_Speed_Slow);
            
        
        while(OutGateButtonC.isPressed()==LOW){delay(5);/*Serial.println("Schleife 2")*/;}
        if(OutGateButtonC.isPressed()){
            evshield.bank_b.motorStop(SH_Motor_1,SH_Next_Action_BrakeHold);delay(1000);
            evshield.bank_b.motorReset();
            //Serial.println("Motor Stopt 2");
            
         }
         IDOUT=false;
         OutGate=IDLESTATE; break;
                                 }
                                 

//default: OutGate = IDLESTATE; 
 
}


    
}
//___________________________________________________________________________________________________________________________________________________________________________________________
void InObjectScan(){
    
    DetectedObjectIn = digitalRead(InSensor);    if (DetectedObjectIn == LOW)
    {
        Serial.println("Fahrweg blockiert");
    }
    if(DetectedObjectIn == HIGH)
    {
        Serial.println("Fahrweg frei");
    }
    delay(200);
}
void OutObjectScan(){
    
    DetectedObjectOut = digitalRead(OutSensor);
    if (DetectedObjectOut == LOW)
    {
        Serial.println("Fahrweg blockiert");
    }
    if(DetectedObjectOut == HIGH)
    {
        Serial.println("Fahrweg frei");
    }
    delay(200);
}

//___________________________________________________________________________________________________________________________________________________________________________________________
void CardScan(){
  //byte In_UID[4]={mfrc522[0].uid.uidByte[0], mfrc522[0].uid.uidByte[1], mfrc522[0].uid.uidByte[2],mfrc522[0].uid.uidByte[3]};
  //byte Out_UID[4]={mfrc522[1].uid.uidByte[0], mfrc522[1].uid.uidByte[1], mfrc522[1].uid.uidByte[2],mfrc522[1].uid.uidByte[3]};
  for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
 
    // Look for new cards
    /*Serial.print(In_UID[0]);
    Serial.print(In_UID[1]);
    Serial.print(In_UID[2]);
    Serial.print(In_UID[3]);
    Serial.println();
    Serial.print(Out_UID[0]);
    Serial.print(Out_UID[1]);
    Serial.print(Out_UID[2]);
    Serial.print(Out_UID[3]);
    Serial.println();
    */
    if (mfrc522[reader].PICC_IsNewCardPresent() && mfrc522[reader].PICC_ReadCardSerial()) {
      MFRC522::PICC_Type piccType = mfrc522[reader].PICC_GetType(mfrc522[reader].uid.sak);
      mfrc522[reader].PICC_HaltA();    
      mfrc522[reader].PCD_StopCrypto1();
     // Serial.println("Card present");
    } 
  } 
 
  if(mfrc522[0].PICC_IsNewCardPresent() && mfrc522[0].PICC_ReadCardSerial()) {
     //Serial.println("Reader 1 Card present");
     for(int i=0; i<4;i++){
     Serial.print(mfrc522[0].uid.uidByte[i]);
     evshield.bank_b.ledSetRGB(0,250,0);
     }
     Serial.println();
     delay(250);
     for(int i=0;i<4;i++){
     mfrc522[0].uid.uidByte[i]=NULL;
     }
     while(!Serial.available()){delay(1000);}
    
     //Serial.println("Read 1");
     setInID(Serial.read());
     evshield.bank_b.ledSetRGB(0,0,0);       
     Serial.end(); 
     Serial.begin(115200); 
     //Serial.println(getInID());
  }
    if(mfrc522[1].PICC_IsNewCardPresent() && mfrc522[1].PICC_ReadCardSerial()) {
     //Serial.println("Reader 2 Card present");
     
     for(int i=0; i<4;i++){
     Serial.print(mfrc522[1].uid.uidByte[i]);
     evshield.bank_b.ledSetRGB(0,0,250);
     }
     Serial.println();
     delay(250);
     for(int i=0;i<4;i++){
      mfrc522[1].uid.uidByte[i]=NULL;
     }
     while(!Serial.available()){delay(1000);}
      
     //Serial.println("Read 2");
     //Serial.println(O);
     setOutID(Serial.read());    
     evshield.bank_b.ledSetRGB(0,0,0);            
     Serial.end();
     Serial.begin(115200); 
     //Serial.println(getOutID());
    }

    
  }
